-------------------------------------------------------------------------------
EditArea for Drupal 5.x
  by Nick Bollweg, Georgia Tech Research Institute 
	 nicholas.bollweg (at) gtri (dot) gatech (dot) edu	
-------------------------------------------------------------------------------

DESCRIPTION
-----------
This module provides Drupal with the EditArea javascript code editor[1].

[1] http://www.cdolivet.net/index.php?page=editArea


REQUIRED COMPONENTS
-------------------
To use EditArea in Drupal, you will need to download EditArea
http://www.cdolivet.net/editarea/

To use the EditArea Compressor, you will need to download
http://joliclic.free.fr/php/javascript-packer/en/

REQUIREMENTS
------------
  - Drupal 5.x
  - PHP 4.3.0 or greater
  - EditArea 1.7.x or greater (http://www.cdolivet.net/index.php?page=editArea)


CONFIGURATION
-------------
   0. Download the EditArea component. Unzip to /modules/editarea/editarea 
      (or /sites/modules/all/editarea/editarea/)
	 1. Enable the module as usual from Drupal's admin pages.
   2. Grant permissions for use of EditArea in Administer > User Management > 
      Access Control

OPTIONAL (BUT RECOMMENDED)
--------------------------
	 0. Download the Packer component. Unzip to /modules/editarea/packer/ 
			(or /sites/modules/all/editarea/packer)



HELP & CONTRIBUTION
-------------------
If you are looking for more information, have any troubles in configuration or if 
you found an issue, please visit the official project page:
  http://drupal.org/project/editarea

We would like to encourage you to join our team if you can help in any way.

If you can translate EditArea module, please use editarea.pot file as a template
(located in "po" directory) and send us the translated file so that we could attach it.

If you can provide new EditArea syntax files, please use the editarea

Any help is appreciated.
     
CREDITS
-------
 - FCKeditor team for template for writing a great editor integration template:
	http://drupal.org/project/fckeditor