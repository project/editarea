<?php
/*
	daevid at daevid dot com
	26-Sep-2007 01:39
	A much simpler way to map the heading/column names to the elements on each line. It also doesn't fill up one big array which could cause you to run out of memory on large datasets. This loads one at a time so you can process/insert to db/etc...
*/
define('LF', "\n");

// Parse a CSV data to a associated 2D array
function csvToArray($path)
{
    $handle = fopen($path, 'r');
		$rows = array();
		if ($handle)
		{
		    set_time_limit(0);

		    //the top line is the field names
		    $fields = fgetcsv($handle, 4096, ',');
		    //loop through one row at a time
		    while (($data = fgetcsv($handle, 4096, ',')) !== FALSE)
		    {
					$rows[] = array_combine($fields, $data);
		    }
		    fclose($handle);
		}
		return $rows;
}